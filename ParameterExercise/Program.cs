﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParameterExercise
{
    class Program
    {
        static void Main(string[] args)
        {
            // Instructions for this exercise:

            // Create two files, each with a separate class. File 1 will hold
            // a class called Car, and the other will hold another class called Factory

            // Car will have the following field variables and properties:

            // Number of Passengers, Make, and Model

            // Factory will have two methods:
            
                // 1) CreateCars, which will take two output parameters of type Car 
                // as arguments, it will create these cars, and then return them.

                // 2) ChangeCars which will take a single car reference as an argument. Change the Make of the car, but dont return anything

            // In the class Program, create an object Factory, and call the CreateCar method and print the Make of one of the cars
            // Then pass that same car into the ChangeCar method and print the Make again. What happened? Why?

        }

      
    }
}
